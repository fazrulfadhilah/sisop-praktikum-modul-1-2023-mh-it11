#!/bin/bash

wget --no-check-certificate "https://drive.google.com/uc?export=download&id=1JYkupCAZTmwPZSZRsB61cklPM-LUjRHp" -O "top_lagu_milik_farrel.csv"

echo -e "5 top lagu teratas dengan genre hip hop:"
grep -i 'hip hop' top_lagu_milik_farrel.csv | sort -t ',' -k15,15nr | head -n 5

echo -e "\n5 lagu terbawah John Mayer:"
grep -i 'John Mayer' top_lagu_milik_farrel.csv | sort -t ',' -k15,15n | head -n 5

echo -e "\n10 lagu teratas tahun 2004:"
grep -i '2004' top_lagu_milik_farrel.csv | sort -t ',' -k15,15n | head -n 10

echo -e "\nLagu ciptaan ibu Sri:"
grep -i 'sri' top_lagu_milik_farrel.csv
