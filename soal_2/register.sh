# Nomor a
read -p "Masukkan username baru : " username

read -p "Masukkan email baru    : " email

# Nomor b
read -s -p "Masukkan password baru : " password

if [ ${#password} -le 8 ]
then
  echo -e "\n\nPassword yang dibuat harus lebih dari 8 karakter"
  echo "[ $(date +'%m/%d/%Y') $(date +'%T')  ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
elif ! [[ "$password" =~ [A-Z] ]] || ! [[ "$password" =~ [a-z] ]]
then
  echo -e "\n\nHarus terdapat paling sedikit 1 huruf kapital dan 1 huruf kecil"
  echo "[ $(date +'%m/%d/%Y') $(date +'%T')  ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
elif [ "$password" = "$username" ]
then
  echo -e "\n\nPassword tidak boleh sama dengan username"
  echo "[ $(date +'%m/%d/%Y') $(date +'%T')  ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
elif ! [[ "$password" =~ [0-9] ]]
then
  echo -e "\n\nHarus terdapat paling tidak 1 angka"
  echo "[ $(date +'%m/%d/%Y') $(date +'%T')  ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
elif ! [[ "$password" =~ [!@#$%\^\&*()] ]]
then
  echo -e "\n\nHarus terdapat paling tidak 1 simbol unik [ !@#$%^&*() ]"
  echo "[ $(date +'%m/%d/%Y') $(date +'%T')  ] [REGISTER FAILED] ERROR Failed register attempt on user $username" >> auth.log
else
  encryptPass=$(echo -n "$password" | base64)

  # Nomor c
  echo "Username : $username, Email : $email, Password : $encryptPass" >> users.txt
  echo "[ $(date +'%m/%d/%Y') $(date +'%T') ] [REGISTER SUCCESS] user $username registered successfully" >> auth.log
  echo -e "\n\nREGISTER SUCCESS - Your account succesfully registered"
fi

